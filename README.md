# HOW TO USE IT

First, please ensure that the image you're using is transparent PNG, and that the faces are white.

Requirements:
- Python 3.8 or newer
- macOS High Sierra or newer OR Linux

Use this command :

    $ bin/cuberegonisio -i PATH_TO_PNG
